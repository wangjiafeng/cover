/**
 * * 微信小程序wxml标签和html5对照obj
 */
module.exports = { 
    'div': {
        tagName: "view"
    },
    'h1': {
        tagName: "text",

    },
    'h2': {
        tagName: "text",

    },
    'p': {
        tagName: "text"
    },
    'span': {
        tagName: "test"
    },
    'script': {
        tagName: "view"
    },
    'a': {
        tagName: "navigator"
    },
    'img': {
        tagName: "image",
        src: "url"
    }

}